package gui;

import controller.Order;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

/**
 * Created by Администратор on 23.12.2018.
 */
public class OrdersList extends VBox {
    ArrayList<Order> ordersList = new ArrayList<Order>();
    VBox parentVBox;
    AnchorPane ordersAnchorPane;
    int ordersCount;
    String ordersListName;
    Button ordersListBtn;
    boolean showCount = true;

    OrdersList (VBox parentVBox, AnchorPane ordersAnchorPane, String ordersListName, Button ordersListBtn) {
        this.ordersListBtn = ordersListBtn;
        this.ordersListName = ordersListName;
        this.ordersAnchorPane = ordersAnchorPane;
        this.parentVBox = parentVBox;
        this.setSpacing(5);
        this.setPadding(new Insets(5,5, 5,5));
        updateBtn();
    }

    private void updateBtn() {
        if (showCount) {
            ordersListBtn.setText(ordersListName + " (" + ordersCount + ")");
        }
        else {
            ordersListBtn.setText(ordersListName);
        }
    }

    public void addOrder(Order order) {
        for (Order order1: ordersList) {
            if (order.getOrderId().equals(order1.getOrderId())) {
                return;
            }
        }
        ordersList.add(order);
        this.getChildren().add(order.getBtnListPane());
        order.getBtnListPane().setOnAction(e -> {
            ordersAnchorPane.getChildren().clear();
            ordersAnchorPane.getChildren().add(order.getPane());
        });
        ordersCount++;
        updateBtn();
    }

    public void removeOrder(String orderId) {
        ordersCount--;
        for (Order order : ordersList) {
            if (order.getOrderId().equals(orderId)) {
                this.getChildren().removeAll(order.getBtnListPane());
                break;
            }

        }
        ordersList.removeIf(order -> order.getOrderId().equals(orderId));
        ordersAnchorPane.getChildren().clear();
        updateBtn();
    }


    public void setShowCount(boolean showCount) {
        this.showCount = showCount;
        updateBtn();
    }


}
