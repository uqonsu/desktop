package gui;

import controller.Controller;
import controller.Order;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import static controller.OrderStatus.CONFIRMED;
import static controller.OrderStatus.PENDING;
import static controller.OrderStatus.READY;

public class MainController {
    @FXML
    private Button btnOrdersWork;

    @FXML
    private Button btnOrdersNew;

    @FXML
    private Button btnOrdersReady;

    @FXML
    private Button btnOrdersHistory;

    @FXML
    private ScrollPane ordersScrollPane;

    @FXML
    private AnchorPane ordersAnchorPane;

    @FXML
    private VBox ordersListVBox;
    private OrdersList ordersListNew;
    private OrdersList ordersListWork;
    private OrdersList ordersListReady;
    private OrdersList ordersListHistory;


    private Button btnTest;

    private OrderPane orderAnchorPane;
    private OrderPane orderAnchorPane2;



    public void initialize() {
        assert btnOrdersWork != null : "fx:id=\"btnOrdersWork\" was not injected: check your FXML file 'main_page.fxml'.";
        assert btnOrdersNew != null : "fx:id=\"btnOrdersNew\" was not injected: check your FXML file 'main_page.fxml'.";
        assert btnOrdersHistory != null : "fx:id=\"btnOrdersHistory\" was not injected: check your FXML file 'main_page.fxml'.";
        assert btnOrdersReady != null : "fx:id=\"btnOrdersReady\" was not injected: check your FXML file 'main_page.fxml'.";
        assert ordersListVBox != null : "fx:id=\"ordersListVBox\" was not injected: check your FXML file 'main_page.fxml'.";
        assert ordersScrollPane != null : "fx:id=\"ordersScrollPane\" was not injected: check your FXML file 'main_page.fxml'.";
        assert ordersAnchorPane != null : "fx:id=\"ordersAnchorPane\" was not injected: check your FXML file 'main_page.fxml'.";
        ordersListNew = new OrdersList(ordersListVBox, ordersAnchorPane, "Новые", btnOrdersNew);
        ordersListWork = new OrdersList(ordersListVBox, ordersAnchorPane, "В работе", btnOrdersWork);
        ordersListReady = new OrdersList(ordersListVBox, ordersAnchorPane, "Выдача", btnOrdersReady);
        ordersListHistory = new OrdersList(ordersListVBox, ordersAnchorPane, "История", btnOrdersHistory);
        ordersListHistory.setShowCount(false);

        btnOrdersWork.setStyle("-fx-background-color: #ffea91");
        btnOrdersNew.setStyle("-fx-background-color: #82adff");
        btnOrdersReady.setStyle("-fx-background-color: #ffd2d2");
        btnOrdersHistory.setStyle("-fx-background-color: #ff6d6d");

        Controller.INSTANCE.setMainController(this);

//        Order testOrder = new Order("Первый заказ", "А05");
        //orderAnchorPane = new OrderPane(testOrder);
      //  Order testOrder2 = new Order("Второй заказ", "D45");
        /*orderAnchorPane2 = new OrderPane(testOrder2);
        orderAnchorPane2.setLayoutX(700);
        orderAnchorPane2.setLayoutY(0);*/

     //   ordersListNew.addOrder(testOrder);
    //    ordersListNew.addOrder(testOrder2);
     //   ordersListNew.removeOrder(testOrder.getOrderId());

     //   ordersListWork.addOrder(testOrder);

        Controller.INSTANCE.startOrdersThread();
        System.out.println("INITED");
    }


    public void addOrder(Order order) {
        if (order.getStatus() == PENDING) {
            ordersListNew.addOrder(order);
        }
        if (order.getStatus() == CONFIRMED) {
            ordersListWork.addOrder(order);
        }
        if (order.getStatus() == READY) {
            ordersListReady.addOrder(order);
        }
    }



    public void buttonNum1click(ActionEvent actionEvent) {
        System.out.println("CLICK!!");
        //btnNum1.setText("Кнопка " + i++);
        ordersAnchorPane.getChildren().add(orderAnchorPane);
        ordersAnchorPane.getChildren().add(orderAnchorPane2);

    }

    public void buttonNum2click(ActionEvent actionEvent) {
        System.out.println("CLICK!!");
        //btnNum1.setText("Кнопка " + i++);
        ordersAnchorPane.getChildren().removeAll(orderAnchorPane);
        ordersAnchorPane.getChildren().removeAll(orderAnchorPane2);


    }


    public void btnOrdersWorkClick(ActionEvent actionEvent) {
        ordersListVBox.getChildren().clear();
        ordersListVBox.getChildren().add(ordersListWork);
    }

    public void btnOrdersNewClick(ActionEvent actionEvent) {
        ordersListVBox.getChildren().clear();
        ordersListVBox.getChildren().add(ordersListNew);
    }

    public void btnOrdersHistoryClick(ActionEvent actionEvent) {
        ordersListVBox.getChildren().clear();
        ordersListVBox.getChildren().add(ordersListHistory);
    }

    public void btnOrdersReadyClick(ActionEvent actionEvent) {
        ordersListVBox.getChildren().clear();
        ordersListVBox.getChildren().add(ordersListReady);
    }


    public OrdersList getOrdersListWork() {
        return ordersListWork;
    }

    public OrdersList getOrdersListNew() {
        return ordersListNew;
    }

    public OrdersList getOrdersListHistory() {
        return ordersListHistory;
    }


    public OrdersList getOrdersListReady() {
        return ordersListReady;
    }
}
