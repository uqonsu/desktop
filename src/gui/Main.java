package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        /*Parent root = FXMLLoader.load(getClass().getResource("main_page.fxml"));
        //MainController cntrl = new MainController();
        primaryStage.setTitle("UQO");
        primaryStage.setScene(new Scene(root, 1200, 700));

        primaryStage.show();*/

        Parent root = FXMLLoader.load(getClass().getResource("login_page.fxml"));
        //MainController cntrl = new MainController();
        primaryStage.setTitle("UQO Login");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 300, 200));

        primaryStage.show();
        //cntrl.initialize();
        
    }


    public static void main(String[] args) {
        launch(args);
    }
}
