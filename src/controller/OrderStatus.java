package controller;

/**
 * Created by Администратор on 18.12.2018.
 */
public enum OrderStatus {
    PENDING,
    CONFIRMED,
    READY,
    EXPIRED,
    CANCELLED,
    ERROR,
    COMPLETED
}
