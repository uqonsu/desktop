package controller;


import gui.LoginController;
import gui.MainController;
import javafx.application.Platform;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Администратор on 18.12.2018.
 */
public class Controller {
    private static String token;
    public static final Controller INSTANCE = new Controller();
    private String ip = "http://benis.site:8000";
    private String shopId;
    private ArrayList<Order> orders = new ArrayList<Order>();
    private OrdersThread ordersThread = new OrdersThread();
    MainController mainController;
    LoginController loginController;
    int found = 0;





    private Controller () {

    }


    void addNewOrder(String id,
                     String customerId,
                     String workerId,
                     String shopId,
                     long status,
                     String created,
                     String orderTime,
                     String order_name,
                     boolean order_paid) {

        for (Order order: orders) {
            if (id.equals(order.getOrderId())) {
                if (status == OrderStatus.CANCELLED.ordinal()+1) {
                    System.out.println("FOUND CANCELED ORDER!!!!");
                    cancelOrder(order);
                }
                if (order.getOrder_paid() != order_paid) {
                    order.setOrder_paid(order_paid);
                }
                return;
            }
        }
        if (status == OrderStatus.CANCELLED.ordinal()+1) return;

        JSONArray ja;
        String       postUrl       = ip + "/order/" + id + "/items/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpGet get          = new HttpGet(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            System.out.println("er1");
            return;
        }
        //get.setEntity(postingString);
        get.setHeader("Authorization", "Bearer " + token);

        try {
            HttpResponse response = httpClient.execute(get);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            ja = (JSONArray) jsonObj.get("items");
            System.out.println(ja.toString());
        } catch (IOException e) {
            System.out.println("er2");
            return;
        } catch (ParseException e) {
            System.out.println("er3");
            return;
        }

        Order order = new Order(id,
                customerId,
                workerId,
                shopId,
                status,
                created,
                orderTime,
                order_name,
                order_paid,
                ja);
        orders.add(order);
        mainController.addOrder(order);
        System.out.println("add new");
        
    }

    public void nextOrderStatus(Order order) {
        int res;
        if (order.getStatus() == OrderStatus.PENDING) {
            if (receiveOrder(order) != 0) {
                System.out.println("RECEIVE ORDER ERROR!!!!!!!!!");
                cancelOrder(order);
                return;
            }
            order.setStatus(OrderStatus.CONFIRMED);
            order.getPane().confirm();
            mainController.getOrdersListNew().removeOrder(order.getOrderId());
            mainController.getOrdersListWork().addOrder(order);
            order.getBtnListPane().setStyle("-fx-background-color: #ffea91");
        } else if (order.getStatus() == OrderStatus.CONFIRMED) {
            if (readyOrder(order) != 0) {
                System.out.println("READY ORDER ERROR!!!!!!!!!");
                cancelOrder(order);
                return;
            }
            order.setStatus(OrderStatus.READY);
            order.getPane().ready();
            mainController.getOrdersListWork().removeOrder(order.getOrderId());
            mainController.getOrdersListReady().addOrder(order);
            order.getBtnListPane().setStyle("-fx-background-color: #ffd2d2");
        } else if (order.getStatus() == OrderStatus.READY) {
            if (!order.getOrder_paid()) {
               res = payOrder(order);
                if (res == 0) {
                    order.setOrder_paid(true);
                } else {
                    System.out.println("PAY ORDER ERROR!!!!!!!!!!");
                }
                return;
            }



            res = completeOrder(order);
            if (res != 0) {
                System.out.println("COMPLETE ORDER ERROR!!!!!!!!!" + res);
                cancelOrder(order);
                return;
            }
            order.setStatus(OrderStatus.COMPLETED);
            order.getPane().complete();
            mainController.getOrdersListReady().removeOrder(order.getOrderId());
            mainController.getOrdersListHistory().addOrder(order);
            order.getBtnListPane().setStyle("-fx-background-color: #ff6d6d");
        }



    }

    public void cancelOrder(Order order) {
        System.out.println("CANCEL ORDER");
        if (order.getStatus() == OrderStatus.PENDING) mainController.getOrdersListNew().removeOrder(order.getOrderId());
        if (order.getStatus() == OrderStatus.CONFIRMED) mainController.getOrdersListWork().removeOrder(order.getOrderId());
        if (order.getStatus() == OrderStatus.READY) mainController.getOrdersListReady().removeOrder(order.getOrderId());
        order.setStatus(OrderStatus.CANCELLED);
        order.getBtnListPane().setStyle("-fx-background-color: #ff6d6d");
        order.getPane().cancel();
        mainController.getOrdersListHistory().addOrder(order);
        cancellOrder(order);
    }




    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public JSONArray getShopsList() {
        JSONArray ja;
        String       postUrl       = ip + "/organization/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String token_ = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpGet get          = new HttpGet(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        //get.setEntity(postingString);
        get.setHeader("Authorization", "Bearer " + token);

        try {
            HttpResponse response = httpClient.execute(get);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            ja = (JSONArray) jsonObj.get("shops");

        } catch (IOException e) {
            return null;
        } catch (ParseException e) {
            return null;
        }
        return ja;
    }

    public int login(String login, String pass) {
        String       postUrl       = ip + "/worker/auth";// put in your url
        JSONObject resultJson = new JSONObject();
        resultJson.put("username", login);
        resultJson.put("password", pass);
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String token_ = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return -2;
        }
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json"); //

        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            token_ = (String) jsonObj.get("token");
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (token_ == null) {
            return -1;
        } else {
            INSTANCE.setToken(token_);
        }
        return 0;
    }


    public int receiveOrder(Order order) {
        String       postUrl       = ip + "/order/" + order.getOrderId() + "/receive/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String res = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return -2;
        }
        post.setEntity(postingString);
        post.setHeader("Authorization", "Bearer " + token);
        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
            res = (String) jsonObj.get("id");
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (res == null) {
            return -1;
        }
        return 0;
    }

    public int readyOrder(Order order) {
        String       postUrl       = ip + "/order/" + order.getOrderId() + "/finish/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String res = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return -2;
        }
        post.setEntity(postingString);
        post.setHeader("Authorization", "Bearer " + token);
        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
            res = (String) jsonObj.get("id");
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (res == null) {
            return -1;
        }
        return 0;
    }

    public int payOrder(Order order) {
        String       postUrl       = ip + "/order/" + order.getOrderId() + "/payment/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String res = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        post.setEntity(postingString);
        post.setHeader("Authorization", "Bearer " + token);
        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (res == null) {
            return -1;
        }
        return 0;
    }


    public int completeOrder(Order order) {
        String       postUrl       = ip + "/order/" + order.getOrderId() + "/done/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String res = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return -2;
        }
        post.setEntity(postingString);
        post.setHeader("Authorization", "Bearer " + token);
        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
            res = (String) jsonObj.get("id");
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (res == null) {
            return -1;
        }
        return 0;
    }

    public int cancellOrder(Order order) {
        String       postUrl       = ip + "/order/" + order.getOrderId() + "/cancel/";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        String res = null;
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return -2;
        }
        post.setEntity(postingString);
        post.setHeader("Authorization", "Bearer " + token);
        try {
            HttpResponse response = httpClient.execute(post);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            System.out.println(json);
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
            res = (String) jsonObj.get("id");
        } catch (IOException e) {
            return -2;
        } catch (ParseException e) {
            return -3;
        }
        if (res == null) {
            return -1;
        }
        return 0;
    }


    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public JSONArray getOrdersList() {
        JSONArray ja;
        String       postUrl       = ip + "/shop/" + this.getShopId() + "/orders/?filter=pending,confirmed,ready,cancelled";// put in your url
        JSONObject resultJson = new JSONObject();
        System.out.println(postUrl);
        System.out.print(resultJson.toString());
        HttpClient httpClient    = HttpClientBuilder.create().build();
        HttpGet get          = new HttpGet(postUrl);
        StringEntity postingString = null;//gson.tojson() converts your pojo to json
        try {
            postingString = new StringEntity(resultJson.toString());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        //get.setEntity(postingString);
        get.setHeader("Authorization", "Bearer " + token);

        try {
            HttpResponse response = httpClient.execute(get);
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(json);
            JSONObject jsonObj = (JSONObject) obj;
            System.out.println(jsonObj.toString());
            ja = (JSONArray) jsonObj.get("orders");
            System.out.println(ja.toString());
        } catch (IOException e) {
            return null;
        } catch (ParseException e) {
            return null;
        }
        return ja;
    }

    public void startOrdersThread() {
        ordersThread.isStopped = false;

        new OrdersThreadStarter().start();
        System.out.println("Starter init");
    }

    private class OrdersThreadStarter extends Thread implements Runnable {
        public OrdersThreadStarter() {

        }
        public void run() {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Start");
            ordersThread.start();
        }

    }

    private class OrdersThread extends Thread implements Runnable {
        JSONArray answer;
        boolean isFirstRun = true;
        boolean isStopped = false;
        public void stopThread() {
            isStopped = true;
        }


        public OrdersThread() {

        }

        @Override
        public void run() {
            while (!isStopped) {
                while (found != 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                answer = INSTANCE.getOrdersList();
                if (answer == null) {
                    System.out.println("Ошибка подключения");
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                if (answer.size() == 0) {
                    System.out.println("No new orders");
                }
                System.out.println(answer.size());
                String a;
                found = answer.size();
                for (int i = 0; i < answer.size(); ++i) {
                    new OrderCreaterThread(((JSONObject) answer.get(i)).get("id") + "",
                            ((JSONObject) answer.get(i)).get("customer_id") + "",
                            ((JSONObject) answer.get(i)).get("worker_id") + "",
                            ((JSONObject) answer.get(i)).get("shop_id") + "",
                            (long) ((JSONObject) answer.get(i)).get("status"),
                            (String) ((JSONObject) answer.get(i)).get("created"),
                            (String) ((JSONObject) answer.get(i)).get("order_time"),
                            (String) ((JSONObject) answer.get(i)).get("order_name"),
                            (boolean) ((JSONObject) answer.get(i)).get("order_paid")
                    ).start();
                }

                try {
                    Thread.sleep(10 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private class OrderCreaterThread extends Thread implements Runnable {
        String id;
        String customerId;
        String workerId;
        String shopId;
        long status;
        String created;
        String orderTime;
        String order_name;
        boolean order_paid;

        public OrderCreaterThread (String id,
                                   String customerId,
                                   String workerId,
                                   String shopId,
                                   long status,
                                   String created,
                                   String orderTime,
                                   String order_name,
                                   boolean order_paid) {
            this.id = id;
            this.customerId = customerId;
            this.workerId = workerId;
            this.shopId = shopId;
            this.status = status;
            this.created = created;
            this.orderTime = orderTime;
            this.order_name = order_name;
            this.order_paid = order_paid;
        }

        @Override
        public void run() {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                        addNewOrder(id,
                                customerId,
                                workerId,
                                shopId,
                                status,
                                created,
                                orderTime,
                                order_name,
                                order_paid);
                    found--;
                }
            });
        }
    }


    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }


    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }


}
