package controller;

import gui.OrderPane;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import org.json.simple.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static controller.OrderStatus.*;

;


public class Order {
    static Font btnFont = new Font(52);
    String[] itemImage;
    String[] itemName;
    int[] itemCount;
    int[] itemPrice;
    int totalPrice;

    String orderName;
    String orderNum;

    OrderPane pane;
    Button btnListPane;





    ///////////////////////////////////
    OrderStatus status = ERROR;
    String orderId = "null";
    String customerId;
    String workerId;
    String shopId;
    String created;
    String orderTime;
    JSONArray items;
    int totalItems;
    String order_name;
    boolean order_paid;


    public Order(String id,
                 String customerId,
                 String workerId,
                 String shopId,
                 long status,
                 String created,
                 String orderTime,
                 String order_name,
                 boolean order_paid,
                 JSONArray items) {
        this.orderId = id;
        this.customerId = customerId;
        this.order_name = order_name;
        this.workerId = workerId;
        this.shopId = shopId;
        this.created = created;
        this.orderTime = orderTime;
        this.items = items;
        this.order_paid = order_paid;
        totalItems = items.size();
        btnListPane = new Button(order_name);
        btnListPane.setPrefSize(275, 100);
        if (status == 1) {
            this.status = PENDING;
            btnListPane.setStyle("-fx-background-color: #82adff");
        }
        if (status == 2) {
            this.status = CONFIRMED;
            btnListPane.setStyle("-fx-background-color: #ffea91");
        }
        if (status == 3) {
            this.status = READY;
            btnListPane.setStyle("-fx-background-color: #ffd2d2");
        }

        btnListPane.setFont(btnFont);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println(timeStamp + " new");
        pane = new OrderPane(this);

        System.out.println(created);
    }

    public JSONArray getItems() {
        return items;
    }

    public void setItems(JSONArray items) {
        this.items = items;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

///////////////////////////////////////////

    public Order(String orderName, String orderNum) {
        this.orderName = orderName;
        this.orderNum = orderNum;
        itemName = new String[totalItems];
        itemImage = new String[totalItems];
        itemCount = new int[totalItems];
        itemPrice = new int[totalItems];


        for (int i = 0; i < totalItems; ++i) {
            itemName[i] = "Название товара" + i + orderName;
            itemCount[i] = i;
            itemPrice[i] = ((i+1) * 5000 ) + ((i+1)*340 % 100) ;
            totalPrice += itemPrice[i]*itemCount[i];
        }

        itemImage[0] = "https://pp.userapi.com/c639123/v639123835/48b6e/A-quhFrbq9c.jpg";
        itemImage[1] = "https://pp.userapi.com/c628722/v628722006/c1dc/U-EYr39Pn8Y.jpg";
        itemImage[2] = "https://sun4-4.userapi.com/c840522/v840522905/8358d/QIzEHNhLDPM.jpg";
        itemImage[3] = "https://pp.userapi.com/c824202/v824202795/ad6d2/T0breB8HaXM.jpg";
        pane = new OrderPane(this);
        btnListPane = new Button(orderNum);
        btnListPane.setPrefSize(275, 100);
        btnListPane.setStyle("-fx-background-color: #82adff");
        btnListPane.setFont(btnFont);
    }



    public int getTotalItems() {
        return totalItems;
    }

    public String getOrderName() {
        return orderName;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public int[] getItemCount() {
        return itemCount;
    }

    public String[] getItemName() {
        return itemName;
    }

    public String[] getItemImage() {
        return itemImage;
    }

    public int[] getItemPrice() {
        return itemPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public OrderPane getPane() {
        return pane;
    }

    public void setPane(OrderPane pane) {
        this.pane = pane;
    }

    public Button getBtnListPane() {
        return btnListPane;
    }

    public void setBtnListPane(Button btnListPane) {
        this.btnListPane = btnListPane;
    }


    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getOrder_name() {
        return order_name;
    }


    public boolean getOrder_paid() {
        return order_paid;
    }



    public void setOrder_paid(boolean order_paid) {
        this.order_paid = order_paid;
        pane.updatePayment(order_paid);

    }


}
