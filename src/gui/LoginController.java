package gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;

/**
 * Created by Администратор on 24.12.2018.
 */
public class LoginController {

    @FXML
    private PasswordField textPassword;

    @FXML
    private TextField textLogin;

    @FXML
    private Label labelError;

    @FXML
    private Button btnLogin;

    @FXML
    private ProgressIndicator prgInd;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label lblLogin;

    @FXML
    private Label lblPass;

    @FXML
    private Label lblChoose;

    @FXML
    private ComboBox comboBox;

    @FXML
    private Button btnChoose;
    private ChooseThread chooseThread;
    private JSONArray shopsList;


    public void initialize() {
        assert textPassword != null : "fx:id=\"textPassword\" was not injected: check your FXML file 'login_page.fxml'.";
        assert textLogin != null : "fx:id=\"textLogin\" was not injected: check your FXML file 'login_page.fxml'.";
        assert btnLogin != null : "fx:id=\"btnLogin\" was not injected: check your FXML file 'login_page.fxml'.";
        assert labelError != null : "fx:id=\"labelError\" was not injected: check your FXML file 'login_page.fxml'.";
        assert prgInd != null : "fx:id=\"prgInd\" was not injected: check your FXML file 'login_page.fxml'.";
        assert gridPane != null : "fx:id=\"gridPane\" was not injected: check your FXML file 'login_page.fxml'.";
        assert lblLogin != null : "fx:id=\"lblLogin\" was not injected: check your FXML file 'login_page.fxml'.";
        assert lblPass != null : "fx:id=\"lblPass\" was not injected: check your FXML file 'login_page.fxml'.";
        assert comboBox != null : "fx:id=\"comboBox\" was not injected: check your FXML file 'login_page.fxml'.";
        assert btnChoose != null : "fx:id=\"btnChoose\" was not injected: check your FXML file 'login_page.fxml'.";
        assert lblChoose != null : "fx:id=\"lblChoose\" was not injected: check your FXML file 'login_page.fxml'.";
    }

    public void btnLoginClick(ActionEvent actionEvent) {
        labelError.setVisible(false);
        btnLogin.setVisible(false);
        prgInd.setVisible(true);
        System.out.println("Login");
        System.out.println(textLogin.getText());
        System.out.println(textPassword.getText());
        new LoginThread(btnLogin, prgInd, labelError, textLogin.getText(), textPassword.getText(), actionEvent).start();

    }

    public void btnChooseClick(ActionEvent actionEvent) {
        int index = comboBox.getSelectionModel().getSelectedIndex();
        if (index < 0) {
            lblChoose.setTextFill(Color.web("#ff0000"));
            return;
        }
        System.out.println((String) ((JSONObject) shopsList.get(index)).get("id"));
        controller.Controller.INSTANCE.setShopId((String) ((JSONObject) shopsList.get(index)).get("id"));
        btnChoose.setVisible(false);
        prgInd.setVisible(true);
        chooseThread = new ChooseThread(actionEvent);
        chooseThread.start();
    }

    private class ChooseThread extends Thread implements Runnable {

        ActionEvent actionEvent;

        public ChooseThread(ActionEvent actionEvent) {
            this.actionEvent = actionEvent;
        }

        @Override
        public void run() {
            try {
                Parent mainWindow = FXMLLoader.load(getClass().getResource("main_page.fxml"));
                Scene mainScene = new Scene(mainWindow, 1200, 700);
                Stage app_stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        app_stage.hide();
                        app_stage.setScene(mainScene);
                        app_stage.setResizable(false);
                        app_stage.setTitle("UQO");
                        app_stage.show();
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class LoginThread extends Thread implements Runnable {
        private Button btn;
        ProgressIndicator prgInd;
        Label labelError;
        String login;
        String password;
        ActionEvent actionEvent;
        public LoginThread(Button btn, ProgressIndicator prgInd, Label labelError, String login, String password, ActionEvent actionEvent) {
            this.btn = btn;
            this.prgInd = prgInd;
            this.labelError = labelError;
            this.login = login;
            this.password = password;
            this.actionEvent = actionEvent;
        }

        @Override
        public void run() {
            int answer = controller.Controller.INSTANCE.login(textLogin.getText(), textPassword.getText());
            if (answer != 0) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (answer == -1) labelError.setText("Неверный логин или пароль");
                        if (answer == -2) labelError.setText("Ошибка подключения к серверу");
                        if (answer == -3) labelError.setText("Ошибка на сервере");
                        labelError.setVisible(true);
                        prgInd.setVisible(false);
                        btnLogin.setVisible(true);
                    }
                });

            } else {
                shopsList = controller.Controller.INSTANCE.getShopsList();

                if (shopsList == null) {
                    labelError.setText("Ошибка получения списка организаций");
                    labelError.setVisible(true);
                    prgInd.setVisible(false);
                    btnLogin.setVisible(true);
                    return;
                }


                int size = shopsList.size();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        labelError.setVisible(false);
                        prgInd.setVisible(false);
                        btnLogin.setVisible(false);
                        lblLogin.setVisible(false);
                        lblPass.setVisible(false);
                        textPassword.setVisible(false);
                        textLogin.setVisible(false);
                        comboBox.setVisible(true);
                        btnChoose.setVisible(true);
                        lblChoose.setVisible(true);
                        for (int i = 0; i < size; ++i) {
                            comboBox.getItems().add(i, (String) ((JSONObject) shopsList.get(i)).get("description"));
                        }
                    }
                });
            }
        }
    }
}
