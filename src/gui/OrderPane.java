package gui;

import controller.Controller;
import controller.Order;
import controller.OrderStatus;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Администратор on 18.12.2018.
 */
public class OrderPane extends AnchorPane {
    private Label orderNum;
    Image image;
    TextArea itemCount;
    TextArea itemPrice;
    TextArea itemName;
    Button nextBtn;
    Button cancelBtn;
    Button historyBtn;
    int totalItems;
    long totalSum = 0;
    long totalItemCount = 0;
    Order order;

    public OrderPane(Order order) {
        this.order = order;
        orderNum = new Label(order.getOrder_name());

        orderNum.setPrefSize(150, 80);
        orderNum.setFont(new Font(50));
        orderNum.setLayoutX(0);
        orderNum.setLayoutY(0);
        this.getChildren().add(orderNum);
        totalItems = order.getTotalItems();
        itemName = new TextArea("Название");
        itemName.setWrapText(true);
        itemName.setLayoutX(170);
        itemName.setLayoutY(85);
        itemName.setPrefSize(350, 15);
        itemName.setEditable(false);
        itemName.setFont(new Font(12));
        this.getChildren().add(itemName);
        itemCount = new TextArea("Кол-во");
        //itemCount.setWrapText(true);
        itemCount.setLayoutX(525);
        itemCount.setLayoutY(85);
        itemCount.setPrefSize(60, 15);
        itemCount.setFont(new Font(11));
        itemCount.setEditable(false);
        this.getChildren().add(itemCount);

        itemPrice = new TextArea("Цена");
        itemPrice.setWrapText(true);
        itemPrice.setLayoutX(590);
        itemPrice.setLayoutY(85);
        itemPrice.setPrefSize(100, 15);
        itemPrice.setFont(new Font(11));
        itemPrice.setEditable(false);
        this.getChildren().add(itemPrice);

        for (int i = 0; i < order.getTotalItems(); ++i) {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            System.out.println(timeStamp + " created");
            Image image = new Image( (String) ((JSONObject)((JSONObject)(order.getItems().get(i))).get("image")).get("medium"), 150, 100, false, true);
            ImageView imageView = new ImageView(image);
            imageView.setLayoutX(0);
            imageView.setLayoutY(125 + 110*i);
            this.getChildren().add(imageView);

            itemName = new TextArea((String) ((JSONObject)(order.getItems().get(i))).get("name"));
            itemName.setWrapText(true);
            itemName.setLayoutX(170);
            itemName.setLayoutY(125 + 110*i);
            itemName.setPrefSize(350, 100);
            itemName.setEditable(false);
            itemName.setFont(new Font(15));
            this.getChildren().add(itemName);


            itemCount = new TextArea("" + ((JSONObject)(order.getItems().get(i))).get("amount"));
            totalItemCount += (long) ((JSONObject)(order.getItems().get(i))).get("amount");
            itemCount.setWrapText(true);
            itemCount.setLayoutX(525);
            itemCount.setLayoutY(125 + 110*i);
            itemCount.setPrefSize(50, 100);
            itemCount.setFont(new Font(18));
            itemCount.setEditable(false);
            this.getChildren().add(itemCount);

            long price = (long) ((JSONObject)(order.getItems().get(i))).get("price");
            totalSum += price;
            itemPrice = new TextArea(price/100 + "." + price%100);
            itemPrice.setWrapText(true);
            itemPrice.setLayoutX(590);
            itemPrice.setLayoutY(125 + 110*i);
            itemPrice.setPrefSize(100, 100);
            itemPrice.setFont(new Font(18));
            itemPrice.setEditable(false);
            this.getChildren().add(itemPrice);

        }
        itemCount = new TextArea("" + totalItemCount);
        itemCount.setWrapText(true);
        itemCount.setLayoutX(525);
        itemCount.setLayoutY(125 + 110*order.getTotalItems());
        itemCount.setPrefSize(50, 50);
        itemCount.setFont(new Font(18));
        itemCount.setEditable(false);
        this.getChildren().add(itemCount);


        itemPrice = new TextArea("" + totalSum/100 + "." + totalSum%100);
        if (order.getOrder_paid()) {
            itemPrice.setStyle("-fx-background-color: #00ff00;");
        } else {
            itemPrice.setStyle("-fx-background-color: #ff0000;");
        }
        itemPrice.setWrapText(true);
        itemPrice.setLayoutX(590);
        itemPrice.setLayoutY(125 + 110*order.getTotalItems());
        itemPrice.setPrefSize(100, 50);
        itemPrice.setFont(new Font(18));
        itemPrice.setEditable(false);
        this.getChildren().add(itemPrice);


        nextBtn = new Button();
        nextBtn.setMinSize(0,55);
        nextBtn.setPrefSize(0, 55);
        nextBtn.setLayoutX(0);
        nextBtn.setLayoutY(195 + 110*order.getTotalItems());
        this.getChildren().add(nextBtn);

        nextBtn = new Button();

        nextBtn.setLayoutX(5);
        nextBtn.setLayoutY(195 + 110*order.getTotalItems());
        if (order.getStatus() == OrderStatus.PENDING) nextBtn.setText("Принять заказ");
        if (order.getStatus() == OrderStatus.READY) {
            if (order.getOrder_paid()) {
                nextBtn.setText("Выдать заказ");
            } else
            {
                nextBtn.setText("Клиент оплатил заказ наличными");
            }

        }
        if (order.getStatus() == OrderStatus.CONFIRMED) nextBtn.setText("Завершить заказ");
        if (order.getStatus() == OrderStatus.CANCELLED) {
            historyBtn = new Button();
            historyBtn.setLayoutX(5);
            historyBtn.setLayoutY(195 + 110*order.getTotalItems());
            historyBtn.setText("Отменен");
            historyBtn.setPrefSize(870, 50);
            this.getChildren().add(historyBtn);
            return;
        }
        nextBtn.setPrefSize(435, 50);
        nextBtn.setOnAction(e -> {
            Controller.INSTANCE.nextOrderStatus(order);

        });
        this.getChildren().add(nextBtn);

        cancelBtn = new Button();
        cancelBtn.setPrefSize(435, 50);
        cancelBtn.setText("Отменить заказ");

        cancelBtn.setLayoutX(445);
        cancelBtn.setLayoutY(195 + 110*order.getTotalItems());
        cancelBtn.setOnAction(e -> {
            Controller.INSTANCE.cancelOrder(order);
        });
        this.getChildren().add(cancelBtn);


    }


    public void updatePayment(boolean isPaid) {
        if (isPaid) {
            itemPrice.setStyle("-fx-background-color: #00ff00;");
            nextBtn.setText("Выдать заказ");
        } else {
            itemPrice.setStyle("-fx-background-color: #ff0000;");
            nextBtn.setText("Клиент оплатил заказ наличными");
        }
    }

    public void confirm() {
        nextBtn.setText("Заказ клиента готов");
    }

    public void complete() {
        historyBtn = new Button();
        historyBtn.setLayoutX(5);
        historyBtn.setLayoutY(195 + 110*totalItems);
        historyBtn.setText("Завершен");
        historyBtn.setPrefSize(870, 50);
        this.getChildren().removeAll(cancelBtn, nextBtn);
        this.getChildren().add(historyBtn);
    }

    public void cancel() {
        historyBtn = new Button();
        historyBtn.setLayoutX(5);
        historyBtn.setLayoutY(195 + 110*totalItems);
        historyBtn.setText("Отменен");
        historyBtn.setPrefSize(870, 50);
        this.getChildren().removeAll(cancelBtn, nextBtn);
        this.getChildren().add(historyBtn);
    }

    public void ready() {
        if (order.getOrder_paid()) {
            nextBtn.setText("Выдать заказ");
        } else
        {
            nextBtn.setText("Клиент оплатил заказ наличными");
        }
    }

   /* private OrderPane(Order order, int j) {
        orderNum = new Label(order.getOrderNum());
        orderNum.setPrefSize(150, 80);
        orderNum.setFont(new Font(50));
        orderNum.setLayoutX(0);
        orderNum.setLayoutY(0);
        this.getChildren().add(orderNum);

        for (int i = 0; i < order.getTotalItems(); ++i) {
            Image image = new Image(order.getItemImage()[i], 150, 100, false, true);
            ImageView imageView = new ImageView(image);
            imageView.setLayoutX(0);
            imageView.setLayoutY(100 + 110*i);
            this.getChildren().add(imageView);

            itemName = new Text(order.getItemName()[i]);
           // itemName.setTextAlignment(TextAlignment.CENTER);
            //itemName.setX(150);
            //itemName.setY(200 + 110*i);
            itemName.setFont(new Font(15));
            this.getChildren().add(itemName);

            itemCount = new Text("" + order.getItemCount()[i]);
            itemCount.setTextAlignment(TextAlignment.CENTER);
            itemCount.setX(400);
            itemCount.setY(200 + 110*i);
            itemCount.setFont(new Font(20));
            this.getChildren().add(itemCount);

            itemPrice = new Text("" + order.getItemPrice()[i]);
            itemPrice.setTextAlignment(TextAlignment.CENTER);
            itemPrice.setX(440);
            itemPrice.setY(200 + 110*i);
            itemPrice.setFont(new Font(20));
            this.getChildren().add(itemPrice);

        }
    }*/



}
